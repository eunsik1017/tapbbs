-- 코멘트 존재글 삭제 차단
INSERT INTO tb_setting (parameter, value, default_bbs, exec_user_idx, client_ip) VALUES ('block_delete_has_comment', 0, 1, (SELECT idx FROM tb_users WHERE user_id = 'admin'), 'BUILT IN');
INSERT INTO tb_setting (parameter, value, default_bbs, exec_user_idx, client_ip) VALUES ('block_delete_has_comment_msg', '댓글이 존재하는 글은 삭제할 수 없습니다.', 1, (SELECT idx FROM tb_users WHERE user_id = 'admin'), 'BUILT IN');

-- 코멘트 존재글 삭제 차단 (기존 게시판 설정에 추가)
INSERT INTO tb_bbs_setting (SELECT NULL, idx, 'block_delete_has_comment', 0, (SELECT idx FROM tb_users WHERE user_id = 'admin'), 'BUILT IN' FROM tb_bbs);
INSERT INTO tb_bbs_setting (SELECT NULL, idx, 'block_delete_has_comment_msg', '댓글이 존재하는 글은 삭제할 수 없습니다.', (SELECT idx FROM tb_users WHERE user_id = 'admin'), 'BUILT IN' FROM tb_bbs);